export default {
  init() {
    // JavaScript to be fired on all pages
    // hamburgers js
    // -------------------------------------------------------

    let solapa = $('.nav-primary');

    $('.hamburger').click(function() {
      if ($(this).hasClass('is-active')) {
        $(this).removeClass('is-active');
        //solapa.removeClass('abierto');
        solapa.animate({
          'right' : '-400px',
        }, 500)
      } else {
        $(this).addClass('is-active');
       // solapa.addClass('abierto');
        solapa.animate({
          'right' : 0,
        }, 500)
      }
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
