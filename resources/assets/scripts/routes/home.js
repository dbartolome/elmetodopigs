export default {
  init() {
    // JavaScript to be fired on the home page
    $('.btnPlay').on('click', function(){
      $('.capaVideo').fadeIn();
    });
    $('.btnCerrar').on('click', function(){
      $('.capaVideo').fadeOut();
    })
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
