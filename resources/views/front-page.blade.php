@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row align-items-center altoMax">
    <div class="col-12 centrado">
      <img src="wp-content/themes/elmetodopigs/dist/images/Human_pig.svg" class="logoMetodo">
      <h1 class="titMetodo">El método Pigs</h1>
      <div class="descMetodo">Un cortometraje de <strong>Boris Kozlov</strong></div>
    </div>
    <div class="col-12 centrado">
      <div class="parteInferior">
        @php dynamic_sidebar('idioma') @endphp
        @php dynamic_sidebar('redes-home') @endphp
      </div>
    </div>
  </div>
</div>
<!--
<div class="btnPlay"><img src="/wp-content/themes/elmetodopigs/dist/images/btnVideo.png"></div>
<div class="capaVideo">
  <div class="btnCerrar"><img src="/wp-content/themes/elmetodopigs/dist/images/btnCerrar.png"></div>
<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/345163712?color=f3e066&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
</div> -->
@endsection
