
@extends('layouts.app')
@section('content')
  <div class="row">
  @while(have_posts()) @php the_post() @endphp
    <div class="col-12 col-sm-6" >
  @include('partials.content-categorynoticias')
    </div>
  @endwhile
  </div>
@endsection
