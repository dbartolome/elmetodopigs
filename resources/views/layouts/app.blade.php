<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @if (is_home())
      @include('partials.headerHome')
      <div class="wrap container" role="document">
            @yield('content')
      </div>
    @else
      @include('partials.header')
          <div class="wrap container interiores" role="document">
            <!-- <div class="content"> -->
               <main class="main">
                @yield('content')
              </main>
           {{--   @if (App\display_sidebar())
                <aside class="sidebar">
                  @include('partials.sidebar')
                </aside>
              @endif --}}
            <!-- </div> -->
            <div class="container-fluid">
              <div class="row">
                <div class="col-12 centrado">
                  <div class="parteInferior">
                    @php dynamic_sidebar('idioma') @endphp
                    @php dynamic_sidebar('redes-home') @endphp
                  </div>
                </div>
              </div>
            </div>
          </div>
    @endif

    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
