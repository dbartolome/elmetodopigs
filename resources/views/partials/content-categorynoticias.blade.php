

  <article @php post_class() @endphp>
    <header>
      @php if(get_field('desarrollada')){ @endphp
      <img src="{{ the_post_thumbnail_url( "100%") }}" width="100%">
      <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
      {{-- @include('partials/entry-meta') --}}
    </header>
    <div class="entry-summary">
      @php the_excerpt() @endphp
    </div>
    @php } else{ @endphp
    <img src="{{ the_post_thumbnail_url( "100%") }}" width="100%">
    <h2 class="entry-title">{!! get_the_title() !!}</h2>
    {{-- @include('partials/entry-meta') --}}
    </header>
    <div class="entry-summary">
      @php the_content() @endphp
    </div>
    @php } @endphp
  </article>

