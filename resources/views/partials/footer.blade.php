<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-sm-3"> @php dynamic_sidebar('sidebar-footer-sup1') @endphp</div>
      <div class="col-sm-3"> @php dynamic_sidebar('sidebar-footer-sup2') @endphp</div>
      <div class="col-sm-3"> @php dynamic_sidebar('sidebar-footer-sup3') @endphp</div>
      <div class="col-sm-3"> @php dynamic_sidebar('sidebar-footer-sup4') @endphp</div>
    </div>
  </div>
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>
</footer>
