{{--
  Template Name: Template Creditos
--}}

@extends('layouts.app')


@section('content')
  @include('partials.page-header')
  <div class="contDireccion">
    <div class="puestoCreditos">Dirección</div><div class="nomCreditos">Boris Kozlov</div>
    <div class="puestoCreditos">Guión</div> <div class="nomCreditos">Boris Kozlov   Zoe Berriatúa</div>
    <div class="puestoCreditos">Una producción de</div> <div class="nomCreditos">Kozlov Media   Bravo Tango Zulú   José Calero   Pablo Heras</div>
  </div>
  <div class="contReparto">
    <div class="puestoCreditos">Reparto</div>
    <div class="nomActor">César Camino </div> <div class="nomPersonaje">Toni</div><br />
      <div class="nomActor">Carmen Del Conte </div> <div class="nomPersonaje"> Tania</div><br />
      <div class="nomActor">Christian Checa </div> <div class="nomPersonaje"> Jon</div><br />
      <div class="nomActor">Noemi Hopper </div> <div class="nomPersonaje"> Caty</div><br />
      <div class="nomActor">Robert Crumpton</div> <div class="nomPersonaje"> Jeff</div><br />
      <div class="nomActor">Carmela Lloret </div> <div class="nomPersonaje"> Pasajera Uber </div><br />
      <div class="nomActor">Héctor Kozlov </div> <div class="nomPersonaje"> Hijo</div><br />
      <div class="nomActor">Olivia Calero </div> <div class="nomPersonaje"> Hija</div><br />
      <div class="nomActor">Ace Andrew Dayag Betonio </div> <div class="nomPersonaje"> Cliente de Tania</div><br />
      <div class="nomActor">Lucas Heras </div> <div class="nomPersonaje"> Cliente de Toni</div><br />
      <div class="nomActor">Chen Lun </div> <div class="nomPersonaje"> Clienta Airbnb</div><br />
      <div class="nomActor">Yan Huang </div> <div class="nomPersonaje"> Clienta Airbnb</div><br />
      <div class="nomActor">Gonzalo Trujillo </div> <div class="nomPersonaje"> Mark Taire</div><br />
  </div>
  <div class="contOtros">
  <div class="puestoCreditos">Dirección de casting</div> <div class="nomCreditos">Jimena Burgueño</div>
  <div class="puestoCreditos">Jefa de producción</div><div class="nomCreditos">Noelia Fernandez</div>
  <div class="puestoCreditos">Localizaciones</div>
  <div class="nomCreditos">Belén Rodriguez<br />
    Marcela Correa<br />
    Victoria Ovejero</div>
  <div class="puestoCreditos">Contabilidad y administración</div> <div class="nomCreditos">
    César Vázquez  (Bravo Tango Zulú)</div>
  <div class="puestoCreditos">Ayudante de producción</div>
  <div class="nomCreditos"> Brezo Davis<br />
    Daniel Brieva</div>
  <div class="puestoCreditos">1er Ayudante de dirección</div>
  <div class="nomCreditos">José Calero</div>

  <div class="puestoCreditos">2º Ayudante de dirección</div>
  <div class="nomCreditos">Jimena Burgueño</div>

  <div class="puestoCreditos">Director de fotografía</div>
  <div class="nomCreditos">Imanol Nabea</div>

  <div class="puestoCreditos">Ayudante de cámara</div>
  <div class="nomCreditos">Gonzalo Asensio</div>

  <div class="puestoCreditos">Auxiliar de cámara</div>
  <div class="nomCreditos">Álvaro de la Vallina</div>

  <div class="puestoCreditos">D.I.T.</div>
  <div class="nomCreditos">Andrea Vitores</div>

  <div class="puestoCreditos">Gafer</div>
  <div class="nomCreditos">Emilio Fuidia</div>

  <div class="puestoCreditos">Key grip</div> <div class="nomCreditos">
    Paco Sánchez</div>

  <div class="puestoCreditos">Electricos</div>
  <div class="nomCreditos">Sergio Fuidia<br />
    Alberto Morales<br />
    David Guerra<br />
    José Varas</div>



  <div class="puestoCreditos">Director de arte</div>
  <div class="nomCreditos">Diego Sánchez</div>

  <div class="puestoCreditos">Ayudante de arte</div>
  <div class="nomCreditos">Verónica Del Valle Dichy</div>

  <div class="puestoCreditos">Atrezzistas</div>
  <div class="nomCreditos">Jorge Acero<br />
    Roger Vargas<br />
    Iván Navarro</div>

  <div class="puestoCreditos">DIBUJANTE</div> <div class="nomCreditos">
    Jesús Colomina Orgaz “Colo” </div>

  <div class="puestoCreditos">Storyboard</div>
  <div class="nomCreditos">Carlos Yllana</div>

  <div class="puestoCreditos">Música original</div>
  <div class="nomCreditos">Pedro E. Fainguersch</div>

  <div class="puestoCreditos">Vestuario</div>
  <div class="nomCreditos">Ester Lucas</div>

  <div class="puestoCreditos">Ayudante estilismo</div>
  <div class="nomCreditos">Beatriz Lorenzo</div>

  <div class="puestoCreditos">Maquillaje y peluquería</div>
  <div class="nomCreditos">Verónica Almendros</div>

  <div class="puestoCreditos">Montaje</div>
  <div class="nomCreditos">Carlos Egea<br />
    Álvaro de la Hoz</div>

  <div class="puestoCreditos">Efectos VFX</div>
  <div class="nomCreditos">Lucas Heras</div>

  <div class="puestoCreditos">Artista 3D</div>
  <div class="nomCreditos">Vanessa González</div>

  <div class="puestoCreditos">Etalonaje</div>
  <div class="nomCreditos">Andrea Vitores</div>


  <div class="puestoCreditos">Sonido directo</div>
  <div class="nomCreditos">Aitor Berenguer<br />
    Mateo Menendez<br />
    Luis Ortega</div>

  <div class="puestoCreditos">Diseño de sonido y mezclas</div>
  <div class="nomCreditos">Fede Pájaro </div>

  <div class="puestoCreditos">Foley</div>
  <div class="nomCreditos">Alberto Álvarez Regidor<br />
    Carlos Riera</div>

  <div class="puestoCreditos">Postproducción de audio</div>
  <div class="nomCreditos">The Lobby Estudios</div>

  <div class="puestoCreditos">Mezclador músicas</div>
  <div class="nomCreditos">Oswaldo Terrones </div>


  <div class="puestoCreditos">Diseños UX</div>
  <div class="nomCreditos">José Maraver
    Jon Pittaluga</div>

  <div class="puestoCreditos">Diseño libros</div>
  <div class="nomCreditos">José Maraver
    Ernesto Ramíerez</div>

  <div class="puestoCreditos">Diseño créditos web y poster</div>
  <div class="nomCreditos">José Maraver</div>

  <div class="puestoCreditos">Programador web</div>
  <div class="nomCreditos">Diego Bartolomé</div>


</div>
<div class="contProveedores">
  <div class="puestoCreditos">Proveedores</div>

    <div class="tipoProveedor">Material Eléctrico </div>--<div class="nomProveedor"> Madcrew</div><br />
    <div class="tipoProveedor">Material de Cámara </div>--<div class="nomProveedor"> Camara Service</div><br />
    <div class="tipoProveedor">Estudio de sonido </div>--<div class="nomProveedor"> The Lobby</div><br />
    <div class="tipoProveedor">Camara Car </div>--<div class="nomProveedor">  CC Car</div><br />
    <div class="tipoProveedor">Walkie Talkies </div>--<div class="nomProveedor">  Menos Doce DB, A tempo.</div><br />
    <div class="tipoProveedor">Transportes </div>--<div class="nomProveedor"> Ari Transportes, Transportes Sicu, Go World</div><br />
    <div class="tipoProveedor">Catering </div>--<div class="nomProveedor"> Plan C</div><br />
    <div class="tipoProveedor">Material de Sonido </div>--<div class="nomProveedor"> Soundrec SL, Reptil Studio</div><br />
    <div class="tipoProveedor">Caravanas de Maquillaje </div>--<div class="nomProveedor"> Classic Camper<br />
      <div class="tipoProveedor">Alquiler de Atrezo </div>--<div class="nomProveedor"> Prop Arte Store</div><br />
    <div class="tipoProveedor">Reservas de Espacio </div>--<div class="nomProveedor"> Space Division, La Reserva</div><br />
        <div class="tipoProveedor">Gestoría </div>--<div class="nomProveedor"> CAGM</div><br />
        <div class="tipoProveedor">Seguros </div>--<div class="nomProveedor"> Montgomery Kent</div><br />
  </div>
</div>
  <div class="titDistricucion">Distribución</div>
  <div class="contDistribucion">
    <img src="/wp-content/themes/elmetodopigs/dist/images/logo_selected.png">
  </div>

  <div class="titAgradecimeintos"> Agradecimiento a los co-financiadores y backers en Kickstarter</div>

  <div class="container centrado">
    <div class="row">
      <div class="col-sm-4">
        Aleksandar Kozlov<br />
        Cándida Montalvo<br />
        Jesús Valero<br />
        Jimena Burgueño<br />
        Martin Bravo<br />
        Carlota Coronado<br />
        José María Carrasco<br />
        Ignacio Castro Rey<br />
        Jorge Úbeda<br />
        Raquel González<br />
        Antonio González<br />
        Montse Martínez<br />
        Alberto Montalvo<br />
        Laura Soto González<br />
        José Luis Montalvo<br />
        Alfonso Valero<br />
        Adan Levy<br />
        Ricardo Corrochano<br />
        Yamila Fernández Colman<br />
        Eduardo Gutiérrez García<br />
        Claudia C. Pedraza<br />
        Pedro Fainguersch<br />
        Eduardo Carmona<br />
        Miguel Rodriguez Andreu<br />
        Miguel Calderón<br />
        Marta Kozlov<br />
        Ivana Palibrk<br />
        Hana Kanjaa<br />
        Carola Gil<br />

      </div>
      <div class="col-sm-4">
        Dragana Varga Kozlov<br />
        Maja Petrić<br />
        María Barba<br />
        Sebastián Muller<br />
        Nicole Maffeis<br />
        Giovanni Maccelli<br />
        Maja Jousif<br />
        Carlos Yllana<br />
        Emilio Delgado<br />
        José Riello<br />
        Jon Pittaluga<br />
        Verónica Pastrana<br />
        Vero De Luis<br />
        Sebastián Duclert<br />
        Esperanza Martín<br />
        Dacil Castelo<br />
        David Tejedor<br />
        Dario Helman<br />
        Javier Galache<br />
        Alana de la Fuente<br />
        Jorge Maganto<br />
        Martín Bollmann<br />
        Arancha Álvarez<br />
        Javier Truchero<br />
        Raquel Belnichón<br />
        Sarah Kingston<br />
        Mike Ashman<br />
        Mila Pavlov<br />
        Antonin Blanc<br />
        Berta Julián<br />

      </div>
      <div class="col-sm-4">
        Elena Montalvo<br />
        David Pena<br />
        José Calero<br />
        Valeria Sánchez<br />
        Cristina de la Vega<br />
        Rafa Julián<br />
        Carmen Moreno<br />
        Arnaud Brument<br />
        Laura Llamas<br />
        Lucía Villareal<br />
        Ditte Pedersen<br />
        Clara Martínez-Lázaro<br />
        Sindbad Iksel<br />
        Álvaro de la Hoz<br />
        José Vicente<br />
        Samantha Bessega<br />
        Álvaro Giménez Sarmiento<br />
        Elvira Jaén<br />
        Ani Muller<br />
        Ronda Vázquez<br />
        Ángela Rubio<br />
        Eduardo Ruiz<br />
        Jorge Frías<br />
        Marcela Lavezollo<br />
        Milena Panić<br />
        Yaniv Segalovich<br />
        Luis Oliván<br />
        Mónica Rico<br />
        Rodrigo Demirjian<br />

      </div>
    </div>
  </div>
  <div class="titAgradecimeintos">Agradecimientos especiales</div>

  <div class="container centrado">
    <div class="row">
      <div class="col-sm-4">
        Javier Manrique<br />
        Vanessa González<br />
        Juan Beiro<br />
        Pepe Jordana<br />
        Adan Levy<br />
        Marcelo Badimon<br />
        Francisco Palma (TOYOTA España)<br />
        Román Del Real<br />
        Restaurante El Paleto<br />

      </div>
      <div class="col-sm-4">
        Zoe Berriatúa<br />
        Jon Pittaluga<br />
        Isidro Parrondo<br />
        Javier Arias<br />
        Jorge Moreno<br />
        Fernando Moreno<br />
        Pablo Zeta<br />
        Samuel Sánchez<br />
        Eric Foinquinos<br />
        Maggie Left<br />
        Carlos Riviera (TOYOTA  España)<br />
        Familia Montalvo De Luis<br />
        Cajamar (Oficina AZCA)<br />


      </div>
      <div class="col-sm-4">
        Yamila Fernández-Colman<br />
        José Cerós<br />
        David Tejedor<br />
        Carolina Abad-Jara<br />
        Rafael Julián<br />
        Carlos Yllana<br />
        Paloma Llaneza<br />
        Alex Marais<br />
        Impact Hub<br />
        Elena Lázaro<br />
        Marieta Berenguer<br />

      </div>
    </div>
  </div>
  <div class="titAgradecimeintos">Agradecimientos manifestación VTC:</div>

  <div class="container centrado">
    <div class="row">
      <div class="col-sm-4">
        Tomás Ruiz Palancar<br />
        Alberto Montalvo<br />
        José Luis Montalvo<br />
        Elena Montalvo<br />
        Aleksandar Kozlov<br />
        Dragana Varga Kozlov<br />
      </div>
      <div class="col-sm-4">
        Sarah Kingston<br />
        Pedro Fainguersch<br />
        María Barba<br />
        Héctor Kozlov<br />
        Dario Helman<br />
        César Burgueño<br />
      </div>
      <div class="col-sm-4">
        Álvaro Pereira<br />
        Paloma Yañez<br />
        Jorge Frías<br />
        David Pena<br />
        Silvia Gómez<br />


      </div>
    </div>
  </div>
  <div class="contMusica">
    “Acompáñame”<br />
    Rocio Durcal<br />
    © 1966 Universal Music<br />

  </div>
  <div class="contFinal">
    Rodado en la Comunidad de Madrid (España)<br />
    DEPÓSITO LEGAL M-19561-2019<br />
<br />
    Esta obra se encuentra registrada en EGEDA. Obra protegida por<br />
    la entidad de gestión de derechos de los productores audiovisuales.

  </div>
  <div class="container" style="margin-bottom: 50px;">
    <div class="row">
      <div class="col-6" style="text-align: center"><img src="/wp-content/themes/elmetodopigs/dist/images/logoBZK.png"></div>
      <div class="col-6" style="text-align: center"><img src="/wp-content/themes/elmetodopigs/dist/images/logo_kozlov_media.png"></div>
    </div>
  </div>
  @while(have_posts()) @php the_post() @endphp

    @include('partials.content-page')
  @endwhile
@endsection
